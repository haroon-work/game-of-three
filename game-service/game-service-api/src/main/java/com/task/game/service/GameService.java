package com.task.game.service;

/**
 * Interface to play Game of Three.
 *
 * @author Haroon Anwar.
 */
public interface GameService {

  /**
   * It'll play turn of Game of Three.
   *
   * @param number to play on.
   * @return resulting number.
   */
  int playTurn(int number);
}
