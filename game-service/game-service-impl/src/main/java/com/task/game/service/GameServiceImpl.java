package com.task.game.service;

import org.springframework.stereotype.Service;

/**
 * Represent Implementation of Game of Three.
 *
 * @author Haroon Anwar.
 */
@Service
public class GameServiceImpl implements GameService {

  /**
   * {@inheritDoc}
   */
  @Override
  public int playTurn(int number){
    System.out.println("Number to play on: "+number);

    int remainder = number % 3;
    if(remainder == 1){
      System.out.println("Adding -1 to make it divisible by 3");
      number += -1;

    }else if(remainder == 2){
      System.out.println("Adding +1 to make it divisible by 3");
      number += 1;
    }else{
      System.out.println("Adding 0 its already divisible by 3");
    }

    System.out.println("Updated number: "+number);
    int quotient = number / 3;
    System.out.println("Resulting quotient "+quotient);

    if(quotient == 1){
      System.out.println("Hurrah! I've won.");
    }

    return quotient;
  }

}
