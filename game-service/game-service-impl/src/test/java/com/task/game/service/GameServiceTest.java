package com.task.game.service;

import org.junit.Assert;
import org.junit.Test;

/**
 * Test class for {@link com.task.game.service.GameServiceImpl}
 *
 * @author Haroon Anwar.
 */
public class GameServiceTest {

  GameService gameService = new GameServiceImpl();

  @Test
  public void shouldPlusOneToPlayTurn(){
    int number = 56;
    int quotient = gameService.playTurn(number);
    Assert.assertEquals(19, quotient);
  }

  @Test
  public void shouldPlusMinusToPlayTurn(){
    int number = 19;
    int quotient = gameService.playTurn(number);
    Assert.assertEquals(6, quotient);
  }

  @Test
  public void shouldPlusZeroToPlayTurn(){
    int number = 6;
    int quotient = gameService.playTurn(number);
    Assert.assertEquals(2, quotient);
  }
}
