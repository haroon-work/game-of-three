package com.task.game.socket;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import com.task.game.service.GameService;

/**
 * Representing a Game player.
 *
 * @author Haroon Anwar.
 */
public class GamePlayer implements Runnable {

  private Socket socket;
  private GameService gameService;

  public GamePlayer(Socket socket, GameService gameService) {
    this.socket = socket;
    this.gameService = gameService;

    Thread playerThread = new Thread(this);
    playerThread.start();
  }

  /**
   * Runs the player thread.
   */
  public void run() {
    ObjectInputStream ois = null;
    ObjectOutputStream oos = null;

    try {
      ois = new ObjectInputStream(socket.getInputStream());
      oos = new ObjectOutputStream(socket.getOutputStream());

      // Reading
      Integer number = readNumber(ois);
      Integer resultingNumber = gameService.playTurn(number);

      // Writing back
      writeDate(oos, resultingNumber);

    }catch(Exception e) {
      e.printStackTrace();
      writeDate(oos, "Problem Occurred due to Invalid Input or "+e.getMessage());
    }finally {
      try{
        if (ois != null)
          ois.close();
        if (oos != null)
          oos.close();

        socket.close();
      }catch(Exception e){
        e.printStackTrace();
      }
    }
  }

  private Integer readNumber(ObjectInputStream ois) throws IOException, ClassNotFoundException{
    Integer number = (Integer) ois.readObject();
    System.out.println("\n---*****----");
    System.out.println("Number received from other player to play: " + number);
    return number;
  }

  private void writeDate(ObjectOutputStream oos, Object data) {
    try {
      System.out.println("Resulting number sending back: " + data);
      oos.writeObject(data);
    }
    catch(IOException e) {
      e.printStackTrace();
    }
  }

}
