package com.task.game.socket;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import com.task.game.service.GameService;
import com.task.game.service.GameServiceImpl;

/**
 * Socket Server for Game of Three. It'll serve the turn request.
 *
 * @author Haroon Anwar.
 */
public class GameServerSocket {
  private GameService gameService = new GameServiceImpl();
  private ServerSocket server;

  /**
   * constructor.
   */
  public GameServerSocket(Integer port) {
    try {
      server = new ServerSocket(port);
      readyToPlay();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * Listening for other player to play.
   */
  private void readyToPlay() {
    System.out.println("Waiting for other player...");

    while (true) {
      try {
        Socket socket = server.accept();
        new GamePlayer(socket, gameService);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  public static void main(String[] args) {
    try{
      if(args.length == 1) {
        System.out.println("port: " + args[0]);
        Integer port = Integer.valueOf(args[0]);
        new GameServerSocket(port);
      }else {
        System.out.printf("Please provide port only.");
      }
    }catch(Exception e){
      System.out.println("Invalid argument");
      e.printStackTrace();
    }
  }


}
