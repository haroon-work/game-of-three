package com.task.game.web.controller;

import com.task.game.service.GameService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;
import static org.mockito.Mockito.times;

/**
 * Test class for {@link com.task.game.web.controller.GameController}
 *
 * @author Haroon Anwar.
 */
public class GameControllerTest {

  @Mock
  private GameService gameService;

  @InjectMocks
  private GameController gameController;

  @Before
  public void init(){
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void shouldPlayTurn(){
    int number = 18;
    Mockito.when(gameService.playTurn(number)).thenReturn(6);

    ResponseEntity response = gameController.playTurn(number);
    Integer nextNumber = (Integer)response.getBody();

    Mockito.verify(gameService, times(1)).playTurn(number);
    Assert.assertEquals(Integer.valueOf(6), nextNumber);
  }


}
