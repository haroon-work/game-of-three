package com.task.game.web.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Spring Application configurations.
 *
 * @author Haroon Anwar.
 */
@Configuration
@ComponentScan({"com.task.game.web", "com.task.game.service"})
@EnableWebMvc
public class ApplicationConfiguration extends WebMvcConfigurerAdapter {

}

