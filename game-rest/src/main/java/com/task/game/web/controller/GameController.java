package com.task.game.web.controller;

import com.task.game.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST Controller for Game of Three. It'll serve the turn request.
 *
 * @author Haroon Anwar.
 */
@RestController
public class GameController {

  @Autowired
  private GameService gameService;

  /**
   * Serve play turn request from other player.
   *
   * @param number to play on.
   * @return resulting number.
   */
  @RequestMapping(value = "/game/play-turn/{number}", method = RequestMethod.GET)
  public ResponseEntity playTurn(@PathVariable(name = "number") Integer number) {
    System.out.println("\n---*****----");
    System.out.println("Number received from other player to play: "+number);

    int resultingNumber = gameService.playTurn(Math.abs(number));

    System.out.println("Resulting number sending back: "+resultingNumber);
    return new ResponseEntity<Integer>(resultingNumber, HttpStatus.OK);
  }

}
