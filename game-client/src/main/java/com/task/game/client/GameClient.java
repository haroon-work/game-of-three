package com.task.game.client;

import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

import com.task.game.service.GameService;
import com.task.game.service.GameServiceImpl;

/**
 * Client program to play Game of Three.
 *
 * @author Haroon Anwar.
 */
public abstract class GameClient {
  private static int BOUND = 10000;// Upper bound to generate random numbers.
  private GameService gameService = new GameServiceImpl();
  private String hostName;
  private int hostPort;

  public GameClient(String hostName, int hostPort){
    this.hostName = hostName;
    this.hostPort = hostPort;
  }

  /**
   * Starts the Game by incepting a number to second player.
   *
   * @param number to start with.
   */
  private void incept(Integer number) {
    System.out.println("\n---*****----");
    if(number == null){
      Random random = new Random();
      number = Math.abs(random.nextInt(BOUND));
      System.out.println("Auto Generated Random number: "+number);
    }

    while(number > 1){
      number = giveTurn(number);
      if(number ==1){
        System.out.println("Oh! I've lost.");
      }else{
        number = gameService.playTurn(number);
      }
    }
  }

  /**
   * Send number to other player by an API.
   *
   * @param number to send.
   * @return resulting number.
   */
  public abstract int giveTurn(Integer number);

  /**
   * Take player input to take a number to incept.
   */
  private void userInput(){
    int number =0;
    do {
      try{
        System.out.println("Please enter a positive integer. Between 1 to "+BOUND);
        Scanner scanner = new Scanner(System.in);
        number = scanner.nextInt();
      }catch(InputMismatchException e){
        System.out.print("InputMismatch - ");
        number = 0;
      }
    }while (number < 1 || number > BOUND);
    incept(number);
  }

  /**
   * Display game start option.
   */
  private void displayOption() {
    System.out.println();
    System.out.println("(1) Start with auto generated random number.");
    System.out.println("(2) Start with player input number.");
    System.out.println("(3) Exit the Game.");
    System.out.println();
  }

  /**
   * Process selected option.
   *
   * @param optionSelected
   */
  private void processSelectedOption(int optionSelected) {
    switch (optionSelected) {
      case 1:
        this.incept(null);
        break;
      case 2:
        this.userInput();
        break;
      default:
        break;
    }
  }

  public void startGame(){
    System.out.println("Please select one of the below option to start game.");
    int optionSelected =0;
    while(true) {
      try {
        this.displayOption();
        Scanner scanner = new Scanner(System.in);
        optionSelected = scanner.nextInt();

        while (optionSelected < 1 || optionSelected > 3) {
          System.out.print("Please enter a number 1 to 3: ");
          optionSelected = scanner.nextInt();
        }

        if (optionSelected == 3) {
          System.out.println("Exiting game");
          break;
        }
        this.processSelectedOption(optionSelected);
      }catch(InputMismatchException e){
        System.out.print("InputMismatch - Please enter a number 1 to 3: ");
      }catch(Exception e){
        System.out.print("System exception. Looks like other player is not available. Or \n"+e.getMessage());
      }

    }
  }

  public String getHostName() {
    return hostName;
  }

  public int getHostPort() {
    return hostPort;
  }
}
