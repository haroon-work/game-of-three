package com.task.game.client;

import org.springframework.web.client.RestTemplate;

/**
 * REST Client program to play Game of Three.
 *
 * @author Haroon Anwar.
 */
public class RestGameClient extends GameClient{
  private RestTemplate restTemplate = new RestTemplate();
  private String url;

  public RestGameClient(String hostName, int hostPort) {
    super(hostName, hostPort);
    url = getUrl();
    System.out.println("url: "+url);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int giveTurn(Integer number) {
    System.out.println("\n---*****----");
    System.out.println("Sending a number to other player: "+number);

    int resultingNumber =  restTemplate.getForObject(this.url, Integer.class, number);

    System.out.println("Received back from other player: "+resultingNumber);
    return resultingNumber;
  }

  private String getUrl(){
    return new StringBuilder("http://")
        .append(this.getHostName())
        .append(":")
        .append(this.getHostPort())
        .append("/game/play-turn/{number}")
        .toString();
  }

  public static void main(String[] args) {
    try {
      if(args.length == 2) {
        System.out.println("hostName: " + args[0] + ", hostPort: " + args[1]);
        String hostName = args[0];
        Integer hostPort = Integer.valueOf(args[1]);
        RestGameClient client = new RestGameClient(hostName, hostPort);
        client.startGame();
      }else {
        System.out.printf("Please provide Host name and port only.");
      }
    }catch(Exception e){
      System.out.println("Invalid argument");
      e.printStackTrace();
    }
  }
}
