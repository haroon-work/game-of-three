package com.task.game.client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Socket Client program to play Game of Three.
 *
 * @author Haroon Anwar.
 */
public class SocketGameClient extends GameClient{

  public SocketGameClient(String hostName, int hostPort) {
    super(hostName, hostPort);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int giveTurn(Integer number) {
    int resultingNumber = 0;
    ObjectOutputStream oos = null;
    ObjectInputStream ois = null;
    Socket socket = null;
    try {
      // Writing
      System.out.println("\n---*****----");
      System.out.println("Sending a number to other player: "+number);
      socket = new Socket(this.getHostName(), getHostPort());
      oos = new ObjectOutputStream(socket.getOutputStream());
      oos.writeObject(number);

      // Reading
      ois = new ObjectInputStream(socket.getInputStream());
      resultingNumber = (Integer) ois.readObject();

    } catch (UnknownHostException e) {
      throw new RuntimeException("Host is Unknown");
    } catch (IOException e) {
      throw new RuntimeException("Problem while reading/writing on socket.");
    } catch (ClassNotFoundException e) {
      throw new RuntimeException("Invalid valid input.");
    } catch (Exception e) {
      throw new RuntimeException("Error - ");
    }finally {
      try{
        if (ois != null)
          ois.close();
        if (oos != null)
          oos.close();
        if(socket != null)
          socket.close();
      }catch(Exception e){
        e.printStackTrace();
      }
    }
    System.out.println("Received back from other player: "+resultingNumber);
    return resultingNumber;
  }

  public static void main(String[] args) {
    try {
      if(args.length == 2) {
        System.out.println("hostName: " + args[0] + ", hostPort: " + args[1]);
        String hostName = args[0];
        Integer hostPort = Integer.valueOf(args[1]);
        SocketGameClient client = new SocketGameClient(hostName, hostPort);
        client.startGame();
      }else {
        System.out.printf("Please provide Host name and port only.");
      }
    }catch(Exception e){
      System.out.println("Invalid argument");
      e.printStackTrace();
    }
  }
}
