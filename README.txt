
Game of Three
=============

Structure:
Project contains four maven modules.
  1- game-client:
     Contains the client programs. It has two client program for REST based and socket based communication.
  2- game-service:
     Holds the core logic of Game.
  3- game-rest:
     Expose REST interface of Game.
  4- game-socket:
     Expose Socket interface of Game.



How to play:
------------

REST:
  1- Deploy game-rest module on server like tomcat.
  2- Start RestGameClient-jar-with-dependencies by passing argument HOST_NAME and HOST_PORT

Socket:
  1- Start GameServerSocket-jar-with-dependencies by passing argument PORT, it will listen on.
  2- Start SocketGameClient-jar-with-dependencies by passing argument HOST_NAME and HOST_PORT

Jars with dependencies can be generated through maven.

After successfully start client program will prompt for options.

------xx------
Import project into IntelliJ IDEA or eclipse for quick start.

